@extends('layouts.sample_coordinator')


@section('title', 'Bulk Upload APK')

@section('content')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="form-inline" action="/action_page.php">
                        <div class="form-group">
                            <label>Box ID: </label>
                            <input type="password" class="form-control" id="" placeholder="Scan or Type Box">
                        </div>
                        <!-- <div class="form-group">
                             <label for="select_well">Well: </label>
                             <select class="form-control" id="select_well">
                                 <option>Option</option>
                                 <option>2</option>
                                 <option>3</option>
                                 <option>4</option>
                             </select>
                         </div>-->
                        <button type="submit" class="btn btn-primary"> View Box Samples</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>lab_well_id</th>
                            <th>customer_name</th>
                            <th>well_name</th>
                            <th>samp_taken_date</th>

                            <th>BID</th>
                            <th>sample_id</th>
                            <th>box_order</th>
                            <th>tracking_number</th>
                            <th>tracking_number_date</th>
                            <th>analysis_required</th>

                        </tr>
                        </thead>

                        <tbody>
                        @php($i=1)
                        @foreach($result as $res)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$res->lab_well_id}}</td>
                                <td>{{$res->customer_name}}</td>
                                <td>{{$res->well_name}}</td>
                                <td>{{$res->samp_taken_date}}</td>

                                <td>{{$res->BID}}</td>
                                <td>{{$res->SID}}</td>
                                <td>{{$res->box_order}}</td>
                                <td>{{$res->tracking_number}}</td>
                                <td>{{$res->tracking_number_date}}</td>
                                <td>{{$res->analysis_required}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="form-inline" action="/sample/box-value/save">

                        Box_ID (PK),
                        Sample_ID,
                        Box_Order,
                        Tracking_Number,
                        Tracking_Number_Date,
                        Analysis_Required(BOOLIAN)





                        <div class="form-group">
                            <label>Sample_ID ID: </label>
                            <input type="text" class="form-control" placeholder="Scan or Type Box" name="Sample_ID" required>
                        </div>
                        <div class="form-group">
                            <label>Box_Order: </label>
                            <input type="text" class="form-control" placeholder="Scan or Type Box" name="Box_Order" required>
                        </div>
                        <div class="form-group">
                            <label>Tracking_Number : </label>
                            <input type="text" class="form-control" placeholder="Scan or Type Box" name="Tracking_Number" required>
                        </div>
                        <div class="form-group">
                            <label>Tracking_Number_Date: </label>
                            <input type="text" class="form-control" placeholder="Scan or Type Box" name="Tracking_Number_Date" required>
                        </div>
                        <div class="form-group">
                            <label>Analysis_Required </label>
                            <input type="text" class="form-control" placeholder="Scan or Type Box" name="Tracking_Number_Date" required>
                        </div>

                        <button type="submit" class="btn btn-primary"> Add Sample To Box</button>
                    </form>

                </div>
            </div>

        </div>

        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="form-inline" action="#">
                        <div class="form-group">
                            <label>BOX  ORDE: </label>
                            <input type="password" class="form-control" placeholder="#BOX  ORDER">
                        </div>

                        <button type="submit" class="btn btn-primary"> View Box Order</button>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection