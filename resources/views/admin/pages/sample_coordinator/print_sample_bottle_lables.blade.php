@extends('layouts.sample_coordinator')


@section('title', 'Bulk Upload APK')

@section('content')

    <div class="row">
        <div class="col-md-12 mx-auto">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Print Sample Bottle Labels</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="form-inline" action="/action_page.php">

                        <div class="form-group">
                            <label for="select_well">Company: </label>
                            <select class="form-control" id="select_well">
                                <option>Option</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary"> Search</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Company</th>
                            <th>Well Name</th>
                            <th>Engineer</th>
                            <th>Account Manager</th>
                            <th>Project</th>
                            <th>Sample Tacker</th>
                            <th>Sample Coordinator</th>
                            <th>Sample Collected</th>
                            <th>Date</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Data Two</td>
                            <td>Data Three</td>
                            <td>Data Four</td>
                            <td>Data Five</td>
                            <td>Data Six</td>
                            <td>Data Seven</td>
                            <td>Data Eight</td>
                            <td>Nine</td>
                            <td>23.07.2018</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Data Two</td>
                            <td>Data Three</td>
                            <td>Data Four</td>
                            <td>Data Five</td>
                            <td>Data Six</td>
                            <td>Data Seven</td>
                            <td>Data Eight</td>
                            <td>Nine</td>
                            <td>23.07.2018</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Data Two</td>
                            <td>Data Three</td>
                            <td>Data Four</td>
                            <td>Data Five</td>
                            <td>Data Six</td>
                            <td>Data Seven</td>
                            <td>Data Eight</td>
                            <td>Nine</td>
                            <td>23.07.2018</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Data Two</td>
                            <td>Data Three</td>
                            <td>Data Four</td>
                            <td>Data Five</td>
                            <td>Data Six</td>
                            <td>Data Seven</td>
                            <td>Data Eight</td>
                            <td>Data Eight</td>
                            <td>23.07.2018</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <br>

@endsection