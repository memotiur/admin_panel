@extends('layouts.sample_coordinator')


@section('title', 'Bulk Upload APK')

@section('content')

    <div class="row">
        <div class="col-md-12 mx-auto">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add Tracking</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="form-inline" action="#">
                        <div class="form-group">
                            <label>Scan or Type Box ID: </label>
                            <input type="text" class="form-control" placeholder=" Box ID" name="box_id">
                        </div>

                        <button type="submit" class="btn btn-primary"> Add Tracking Number</button>
                    </form>
                    <br>
                    <form class="form-inline" action="#">
                        <div class="form-group">
                            <label>Scan or Type Tracking Number: </label>
                            <input type="password" class="form-control" placeholder=" Tracking Number" name="tracking_number">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
@endsection