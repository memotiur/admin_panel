@extends('layouts.sample_coordinator')


@section('title', 'Bulk Upload APK')

@section('content')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="form-inline" action="/action_page.php">
                        <div class="form-group">
                            <label>Box ID: </label>
                            <input type="password" class="form-control" id="" placeholder="Scan or Type Box">
                        </div>

                        <button type="submit" class="btn btn-primary"> Print Next 20 BOX Labels</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <ul>
                        <li>10001</li>
                        <li>10002</li>
                        <li>10003</li>
                        <li>10004</li>
                        <li>10005</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <br>

@endsection