@extends('layouts.account_manager')


@section('title', 'Admin panel')

@section('content')
    @if(session('success'))
        <div class="alert alert-success">
            <strong></strong> {{session('success')}}
        </div>
    @endif
    @if(session('decline'))
        <div class="alert alert-danger">
            <strong></strong> {{session('decline')}}
        </div>
    @endif
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="" method="post" action="/import/new-well" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">New Well</label>
                            <div class="col-sm-9">
                                <input type="file" readonly class="form-control-plaintext" name="proposal">
                                <input type="file" readonly class="form-control-plaintext" name="proposal_line">
                                <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary"> Upload</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>

        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form class="" method="post" action="/import/daily-data" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Daily Data</label>
                            <div class="col-sm-9">
                                <input type="file"  class="form-control-plaintext" name="daily_data">
                                <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary"> Upload</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection