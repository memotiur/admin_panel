
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Panel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        a{
            color: black;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-md-4 mx-auto text-center" style="padding-top: 70px;">
        <div class="header-title">
            <h4 class="wv-heading--title">
                Login
            </h4>
            <hr>

        </div>
    </div>
    <div class="row divcenter">
        <div class="col-md-4 mx-auto ">
            <div class="card">
                <div class="card-body">
                    <div class="myform form ">
                        <form action="/login-check" method="post" name="login">

                            <div class="form-group">
                                <input type="text" name="email" class="form-control my-input" id="email"
                                       placeholder="Email">
                                <input class="form-control" type="hidden" value="{{csrf_token()}}" name="_token" >
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control my-input"
                                       placeholder="pass">
                            </div>
                            <!-- <div class="form-group">
                                 <select class="form-control" id="select_option">
                                     <option>Account Manager</option>
                                     <option>Lab Account</option>
                                     <option>Sample Co-Ordinator</option>
                                     <option>Tracer Specialist</option>
                                 </select>
                             </div>-->
                            <!--   <div class="text-center ">
                                   <button type="submit" class=" btn btn-block send-button tx-tfm">Login</button>
                               </div>-->

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>
