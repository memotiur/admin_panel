                                                             <!DOCTYPE html>
                                                             <html lang="en">
                                                             <head>
                                                                 <title>@yield('title')</title>
                                                                 <meta charset="utf-8">
                                                                 <meta name="viewport" content="width=device-width, initial-scale=1">
                                                                 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

                                                                 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                                                 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
                                                                 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

                                                                 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
                                                                 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
                                                                 <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
                                                                 <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
                                                                 <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

                                                                 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                                                                 <link rel="stylesheet" href="/css/custom.css">
                                                                 <style>

                                                                 .autocomplete {
                                                                   /*the container must be positioned relative:*/
                                                                   position: relative;
                                                                   display: inline-block;
                                                                 }

                                                                 input {
                                                                   border: 1px solid transparent;
                                                                   background-color: #f1f1f1;
                                                                   font-size: 16px;
                                                                 }

                                                                 input[type=text] {
                                                                   background-color: #f1f1f1;
                                                                   width: 100%;
                                                                 }

                                                                 input[type=submit] {
                                                                   background-color: DodgerBlue;
                                                                   color: #fff;
                                                                   cursor: pointer;
                                                                 }

                                                                 .autocomplete-items {
                                                                   position: absolute;
                                                                   border: 1px solid #d4d4d4;
                                                                   border-bottom: none;
                                                                   border-top: none;
                                                                   z-index: 99;
                                                                   /*position the autocomplete items to be the same width as the container:*/
                                                                   top: 100%;
                                                                   left: 0;
                                                                   right: 0;
                                                                 }

                                                                 .autocomplete-items div {
                                                                   padding: 5px;
                                                                   cursor: pointer;
                                                                   background-color: #fff;
                                                                   border-bottom: 1px solid #d4d4d4;
                                                                 }

                                                                 .autocomplete-items div:hover {
                                                                   /*when hovering an item:*/
                                                                   background-color: #e9e9e9;
                                                                 }

                                                                 .autocomplete-active {
                                                                   /*when navigating through the items using the arrow keys:*/
                                                                   background-color: DodgerBlue !important;
                                                                   color: #ffffff;
                                                                 }
                                                                 </style>

                                                             </head>
                                                             <body>
                                                             <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                                                                 <div class="container">
                                                                     <a class="navbar-brand" href="/admin-home">Tracking System</a>
                                                                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                                                                             aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                                                                         <span class="navbar-toggler-icon"></span>
                                                                     </button>
                                                                     <div class="collapse navbar-collapse" id="navbarCollapse">
                                                                         <ul class="navbar-nav mr-auto">

                                                                             <li class="nav-item">
                                                                                 <a class="nav-link" href="/sample">Sample </a>
                                                                             </li>
                                                                             <li class="nav-item">
                                                                                 <a class="nav-link" href="/wells">Wells</a>
                                                                             </li>
                                                                             <li class="nav-item">
                                                                                 <a class="nav-link" href="/import">Import</a>
                                                                             </li>

                                                                         </ul>
                                                                         <ul class="navbar-nav form-inline mt-2 mt-md-0">
                                                                             <!--<li class="nav-item">
                                                                                 <a class="nav-link" href="import.html">Logout</a>
                                                                             </li>-->
                                                                             <li class="nav-item dropdown">
                                                                                 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                     {{Session::get('user_name')}}
                                                                                 </a>
                                                                                 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                                                     <a class="dropdown-item" href="#">Settings</a>
                                                                                     <div class="dropdown-divider"></div>
                                                                                     <a class="dropdown-item" href="/logout">Logout</a>
                                                                                 </div>
                                                                             </li>
                                                                         </ul>
                                                                     </div>
                                                                 </div>
                                                             </nav>
                                                             <br>
                                                             <br>
                                                             <br>

                                                             <div class="container">

                                                                 @yield('content')

                                                                 <hr>
                                                                 <div class="row">
                                                                     <p>Copyright:..</p>
                                                                 </div>
                                                             </div>

                                                             <script>
                                                                 $(document).ready(function () {
                                                                     $('#example').DataTable();
                                                                 });
                                                             </script>




                                                             </body>
                                                             </html>
