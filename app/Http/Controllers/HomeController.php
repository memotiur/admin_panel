<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /*  public function __construct()
      {
          //$this->middleware('auth');
          if (Auth::check()) {
              return Redirect::to('/admin-home');
              echo "yes";
          }
      }*/

    public function login()
    {

        return view('login.login');
    }

    public function index()
    {
        return view('home');
    }

    public function doLogin(Request $request)
    {
        //return $this->hashSSHA("123");
        $email = $request['email'];
        $password = $request['password'];
        $user = DB::table('mst_user')->where('email', $email)->first();

        if (!is_null($user)) {
            $salt = $user->salt;
            $hash = $this->checkhashSSHA($salt, $password);
             $result = DB::table('mst_user')
                ->join('mst_account_type', 'mst_account_type.AID', '=', 'mst_user.AID')
                ->where('email', $email)->where('password', $hash)->first();
            if (!is_null($result)) {
                Session::put('UID', $result->UID);
                Session::put('user_name', $result->user_name);
                Session::put('email', $result->email);
                Session::put('account_name', $result->account_name);
                if ($result->account_name == "Account Manager") {
                    return Redirect::to('/sample');
                } elseif ($result->account_name == "Sample Coordinator") {
                     return Redirect::to('/check-in-sample');
                } elseif ($result->account_name == "Tracer Specialist") {
                    echo "Tracer";
                } else {
                    echo "else";
                }

                return Redirect::to('/admin-home');
            } else {

                return Redirect::to('/login')->with('username is wrong');
            }

        } else {
            return Redirect::to('/login')->with('username is wrong');
        }

    }

    public function doLogout()
    {
        // Auth::logout(); // log the user out of our application
        Session::flush();
        return Redirect::to('/');
    }

    function checkhashSSHA($salt, $password)
    {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }

    function hashSSHA($password)
    {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1("123456" . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

}
