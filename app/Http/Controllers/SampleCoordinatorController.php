<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SampleCoordinatorController extends Controller
{

    public function CheckInSamples(Request $request)
    {

        /*return DB::table('tbl_box_data')
            ->join('tbl_samp_taken_data','tbl_samp_taken_data.SID','=','tbl_box_data.sample_id')
            ->get();*/
        $result = DB::select('SELECT t.lab_well_id, t.customer_name, t.well_name, t1.samp_taken_date, 
t2.* FROM mst_well t, tbl_samp_taken_data t1, tbl_box_data t2 WHERE t.WID = t1.WID AND t2.SID = t1.SID');


        return view('admin.pages.sample_coordinator.check_in_sample')
            ->with('result', $result);
    }

    public function AddTracking(Request $request)
    {
        //return DB::table('mst_well')->get();
        return view('admin.pages.sample_coordinator.add_tracking');
    }

    public function PrintWellLabels(Request $request)
    {
        //return DB::table('mst_well')->get();
        return view('admin.pages.sample_coordinator.print_well_labels')
            ->with('wells', DB::table('mst_well')->get());
    }

    public function PrintSampleBottle(Request $request)
    {

        return view('admin.pages.sample_coordinator.print_sample_bottle_lables');
    }

    public function PrintBoxlabel(Request $request)
    {

        return view('admin.pages.sample_coordinator.print_sample_bottle_lables');
    }

    public function boxValuesaveFromsamplePage(Request $request)
    {

        return $request->all();
        $request['box_id'];
        //DB::table('tbl_box_data')->create([''])
    }
}
