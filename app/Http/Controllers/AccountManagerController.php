<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Orchestra\Parser\Xml\Facade as XmlParser;


class AccountManagerController extends Controller
{
    //

    public function wells(Request $request)
    {

        /* DB::select("SELECT t1.*, t2.*, t3.*
              FROM mst_well t1
              LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID
              LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID
              WHERE t1.customer_name LIKE '%c%' AND t1.well_name LIKE '%w%'");*/

        $customer = DB::select("SELECT t1.customer_name, t1.well_name, t1.engineer, t1.sales_person, t1.PJID, t1.sample_takers, t1.sample_coordinator, 
            t2.samp_collected_date, t3.samp_taken_date
        
            FROM mst_well t1
            LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID  
            LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID");

        $result = DB::select("SELECT t.customer_name, t.well_name, t.PJID, 
            (SELECT t1.user_name FROM mst_user t1 WHERE t1.UID = t.engineer) AS engineer_name, 
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t.sales_person) AS account_manager_name, 
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t.sample_coordinator) AS coordinator_name, 
            t.WID, t.lab_well_id 
            FROM mst_well t");

        return view('admin.pages.account_manager.wells')
            ->with('users', DB::table('mst_user')->get())
            ->with('customer', $customer)
            ->with('wells', $result);
    }

    public function WellSearch(Request $request)
    {

        /* DB::select("SELECT t1.*, t2.*, t3.*
             FROM mst_well t1
             LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID
             LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID
             WHERE t1.customer_name LIKE '%c%' AND t1.well_name LIKE '%w%'");*/

        $customer = DB::table('mst_well')
            //->join('mst_user','mst_user.UID','=','mst_well.engineer')
            ->get();
        $cus = $request['company'];

        $result = DB::select("SELECT t.customer_name, t.well_name, t.PJID, 
            (SELECT t1.user_name FROM mst_user t1 WHERE t1.UID = t.engineer) AS engineer_name, 
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t.sales_person) AS account_manager_name, 
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t.sample_coordinator) AS coordinator_name, 
            t.WID, t.lab_well_id 
            FROM mst_well t
            WHERE t.customer_name LIKE '%$cus%'");

        return view('admin.pages.account_manager.wells')
            ->with('users', DB::table('mst_user')->get())
            ->with('customer', $customer)
            ->with('wells', $result);
    }

    public function import(Request $request)
    {

        return view('admin.pages.account_manager.import');
    }

    public function sample(Request $request)
    {
        $wells = DB::select("SELECT t1.customer_name, t1.well_name, t1.engineer, t1.sales_person, t1.PJID, t1.sample_takers, t1.sample_coordinator, 
            t2.samp_collected_date, t3.samp_taken_date
        
            FROM mst_well t1
            LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID  
            LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID");
        $result = DB::select("SELECT t1.customer_name, t1.well_name, t1.engineer, t1.sales_person, t1.PJID, t1.sample_takers, t1.sample_coordinator, 
            t2.samp_collected_date, t3.samp_taken_date,
           (SELECT t1.user_name FROM mst_user t1 WHERE t1.UID = t1.engineer) AS engineer_name, 
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t1.sales_person) AS account_manager_name,
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t1.sample_coordinator) AS coordinator_name
            FROM mst_well t1
            LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID  
            LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID");

        return view('admin.pages.account_manager.index')
            ->with('wells', $wells)
            ->with('result', $result);
    }

    public function SampleSearch(Request $request)
    {

        //return $request->all();
        $cus = $request['company'];
        $w = $request['well'];
        $wells = DB::select("SELECT t1.customer_name, t1.well_name, t1.engineer, t1.sales_person, t1.PJID, t1.sample_takers, t1.sample_coordinator, 
            t2.samp_collected_date, t3.samp_taken_date
           
            FROM mst_well t1
            LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID  
            LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID");

        $result = DB::select("SELECT t1.customer_name, t1.well_name, t1.engineer, t1.sales_person, t1.PJID, t1.sample_takers, t1.sample_coordinator, 
            t2.samp_collected_date, t3.samp_taken_date,
           (SELECT t1.user_name FROM mst_user t1 WHERE t1.UID = t1.engineer) AS engineer_name, 
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t1.sales_person) AS account_manager_name,
            (SELECT t2.user_name FROM mst_user t2 WHERE t2.UID = t1.sample_coordinator) AS coordinator_name
            FROM mst_well t1
            LEFT JOIN tbl_samp_collected_data t2 ON t1.WID = t2.WID  
            LEFT JOIN tbl_samp_taken_data t3 ON t1.WID = t3.WID 
            WHERE t1.customer_name LIKE '%$cus%' AND t1.well_name LIKE '%$w%'");

        return back()
            ->with('wells', $wells)
            ->with('result', $result);
    }

    /**
     * @param Request $request
     */
    function DailyData(Request $request)
    {

        if ($request->hasFile('daily_data')) {

            $xml = $request->file('daily_data');
            $xml_name = "xml_" . time() . '.' . $xml->getClientOriginalExtension();
            $destinationPath = public_path('/xml');
            $xml->move($destinationPath, $xml_name);

            $destinationPath . "/" . $xml_name;
            $xml = XmlParser::load($destinationPath . "/" . $xml_name);
            $data = $xml->parse([
                'WELLS' => ['uses' => 'WELL[WELL_ID,WELL_NAME,WELL_TYPE,CUSTOMER_NAME]'],
            ]);

            $counter = 0;

            foreach (json_decode(json_encode($data['WELLS']), true) as $i => $v) {
                echo $v['WELL_ID'] . " | ";
                echo $v['WELL_NAME'] . " | ";
                echo $v['WELL_TYPE'] . " | ";
                echo $v['CUSTOMER_NAME'] . '<br/>';

                $insert_array = array(
                    'well_name' => $v['WELL_NAME'],
                    'district_name' => $v['WELL_TYPE'],
                    'customer_name' => $v['CUSTOMER_NAME'],
                );

                $res = $this->insertWell($insert_array);
                if ($res == 1) {
                    $counter++;
                }

            }
            return back()->with('success', "Successfully entry saved " . $counter);

        } else {
            $xml_name = null;
            return back()->with('failed', "XML is empty ");
        }
    }

    function NewWell(Request $request)
    {
        if ($request->hasFile('proposal')) {

            $xml = $request->file('proposal');
            $xml_name = "xml_" . time() . '.' . $xml->getClientOriginalExtension();
            $destinationPath = public_path('/xml');
            $xml->move($destinationPath, $xml_name);

            $destinationPath . "/" . $xml_name;
            $xml = XmlParser::load($destinationPath . "/" . $xml_name);
           /* $data = $xml->parse([
                'PROPOSAL_LINE' => ['uses' => '
               
               Proposal_ID,Job_Type,Status_ID,Status_Name,Proposal_Date,Proposed_Frac_Date,Country_Name,District_Name,
                SalesPerson,Customer_Name,Engineer,Customer_Address_Line1,Customer_Address_Line2,Customer_City,
                Customer_State,Well_ID,Lab_Well_ID,Well_Name,Well_Type,Well_State,Well_County,
                Service_Company,Service_Company_Location,Field_Contact_Info'],
            ]);*/

            $data = $xml->parse([

                'Proposal_ID' => ['uses','Proposal.Customer_Address_Line1'],
            ]);

return $data;
            $counter = 0;

            foreach (json_decode(json_encode($data['PROPOSAL_LINE']), true) as $i => $v) {
//                /*$insert_array = array(
//                    'well_name' => $v['WELL_NAME'],
//                    'district_name' => $v['WELL_TYPE'],
//                    'customer_name' => $v['CUSTOMER_NAME'],
//                );*/
//
//                //$res = $this->insertWell($insert_array);
//                if ($res == 1) {
//                    $counter++;
//                }

                echo "hello";

            }
            return back()->with('success', "Successfully entry saved " . $counter);

        } else {
            $xml_name = null;
            return back()->with('failed', "XML is empty ");
        }
    }

    private function insertWell($insert_array)
    {
        try {
            DB::table('mst_well')->insert($insert_array);
            return 1;
        } catch (\Exception $exception) {
            return 0;
        }

    }

}
