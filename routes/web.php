<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', function () {
   return "Laravel running";
});
Route::get('/test', function () {
   return \Illuminate\Support\Facades\DB::table('mst_account_type')->get();
});



Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@login');
Route::get('/login', 'HomeController@login');
Route::post('/login-check', 'HomeController@doLogin');
Route::get('/logout', 'HomeController@doLogout');
Route::get('/admin-home', function () {
    return view('admin.pages.home.index');

});

//Account manager
Route::get('/wells', 'AccountManagerController@wells');
Route::get('/import', 'AccountManagerController@import');
Route::get('/sample', 'AccountManagerController@sample');


Route::post('/samples/search', 'AccountManagerController@SampleSearch');
Route::post('/wells/search', 'AccountManagerController@WellSearch');

Route::post('/import/daily-data', 'AccountManagerController@DailyData');
Route::post('/import/new-well', 'AccountManagerController@NewWell');

//SampleCoordinatorController
Route::get('/check-in-sample', 'SampleCoordinatorController@CheckInSamples');
Route::post('/sample/box-value/save', 'SampleCoordinatorController@boxValuesaveFromsamplePage');



Route::get('/add-tracking', 'SampleCoordinatorController@AddTracking');
Route::get('/print-well-labels', 'SampleCoordinatorController@PrintWellLabels');
Route::get('/print-sample-bottle', 'SampleCoordinatorController@PrintSampleBottle');
Route::get('/print-box-labels', 'SampleCoordinatorController@PrintBoxlabel');

